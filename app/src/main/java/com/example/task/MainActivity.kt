package com.example.task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun union(arr1 : Array<Int>, arr2 : Array<Int>) : MutableSet<Int>{

        var newSet = mutableSetOf(arr1[0])
        var i : Int = 1
        var j : Int = 0
        while (i < arr1.size){
            newSet.add(arr1[i])
            i++
        }
        while (j < arr2.size){
            newSet.add(arr2[j])
            j++
        }
        return newSet
    }
    fun intersection(arr1 : Array<Int>, arr2 : Array<Int>) : MutableList<Int>{
        var i : Int = 0
        var j : Int = 0
        var num : Int = 0
        var list = mutableListOf(arr1[0])

        while(i < arr1.size && j < arr2.size){
            if (arr1[i] > arr2[j]){
                j++
            }
            else if (arr2[j] > arr1[i]){
                i++
            }
            else {
                list[num] = arr1[i]
                i++
                j++
            }
            num++
        }
        return list
    }



}